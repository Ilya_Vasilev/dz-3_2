package com;

import java.util.ArrayList;
import java.util.List;

public class Visitor {
    private String visitorName;
    private static int userId = 0;
    private Book book = null;


    public Visitor() {
    }

    public Visitor(String visitorName) {
        this.visitorName = visitorName;

    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public int getUserId() {
        return userId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    List<Visitor> visitorArray = new ArrayList<>();


    public int generateId() {
        return ++userId;
    }


}


