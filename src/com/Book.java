package com;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private String nameOfTheBook;
    private String authorTitle;
    private Visitor visitor = null;
    private double result = 0.0;
    private int grade;

    ArrayList<Integer> scoreArray = new ArrayList<>();


    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public Book() {
    }

    public Book(String nameOfTheBook, String authorTitle) {
        this.nameOfTheBook = nameOfTheBook;
        this.authorTitle = authorTitle;
    }

    public String getNameOfTheBook() {
        return nameOfTheBook;
    }

    public String getAuthorTitle() {
        return authorTitle;
    }

    public void setNameOfTheBook(String nameOfTheBook) {
        this.nameOfTheBook = nameOfTheBook;
    }

    public void setAuthorTitle(String authorTitle) {
        this.authorTitle = authorTitle;
    }


    List<Book> bookArrays = new ArrayList<>();

    public void nameBook() {
        bookArrays.add(new Book("Бородино", "М.Лермонтов"));
        bookArrays.add(new Book("Мастер и Маргарита", "М.Булгаков"));
        bookArrays.add(new Book("Идиот", "Ф.Достоевский"));
        bookArrays.add(new Book("Преступление и наказание", "Ф.Достоевский"));
        bookArrays.add(new Book("А зори здесь тихие", "Б.Васильев"));
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }
}




